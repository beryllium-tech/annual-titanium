using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AnnualTitanium.ClientModels;
using AnnualTitanium.ClientModels.ClientRequests;
using AnnualTitanium.Host.MediatrModels.requests;
using AnnualTitanium.Repository.models;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace AnnualTitanium.Host.Controllers {

  [ApiController]
  [Route("/api/v1/event")]
  public class EventController : ControllerBase {
    private readonly IMediator _mediator;
    private readonly IMapper _mapper;

    public EventController(IMediator mediator, IMapper mapper)
    {
      _mediator = mediator;
      _mapper = mapper;
    }

    /// <summary>
    /// Gets an event from the database and returns the client representation
    /// </summary>
    /// <param name="id">the alphanumeric id</param>
    /// <returns>the client representation of an event item</returns>
    [HttpGet]
    [AllowAnonymous]
    public async Task<IActionResult> GetEvent([FromQuery] string id)
    {
      //create request
      var request = new EventByIdRequest
      {
        Id = id
      };
      
      //send in mediator
      var result = await _mediator.Send(request);

      //check null
      if (result != null)
        return Ok(result);

      return NotFound();
    }

    /// <summary>
    /// Creates an event according to the request specifications
    /// </summary>
    /// <param name="request">The specifications to which the event should be formatted</param>
    /// <returns>Created if successful else BadRequest</returns>
    [HttpPost]
    [AllowAnonymous]
    public async Task<IActionResult> CreateEvent([FromBody]CreateEventRequest request)
    {

      request = request with { When = DateTime.MinValue.AddHours(request.HoursSinceZero)};
      
      //create mediatr request
      var mediatrRequest = _mapper.Map<AddEventRequest>(request);
      
      //send
      var output = await _mediator.Send(mediatrRequest);

      if (output == null)
        return BadRequest();

      
      return Created(HttpContext.Request.Host.Value + "/api/v1/event?id=" + output.Id, output);
    }

    /// <summary>
    /// Deletes an event by its id
    /// </summary>
    /// <param name="eventId">The alphanumeric id of said event</param>
    /// <returns>OK or BadRequest</returns>
    [HttpDelete]
    [AllowAnonymous] //TODO: delete after testing
    public async Task<IActionResult> DeleteEvent([FromQuery] string eventId)
    {
      //convert to request
      var request = new DeleteEventRequest {Id = eventId};
      
      //send to mediatr
      var result = await _mediator.Send(request);

      //return
      if (result)
        return Ok();
      
      return BadRequest("Unable to delete");
    }
    
    
    /// <summary>
    ///   Edits an event to match the model parameter.
    /// 
    ///   DB-item is identified by model id.
    /// </summary>
    /// <param name="model">The model that the db-item will try to conform to</param>
    /// <returns>Ok or BadRequest</returns>
    [HttpPatch]
    public async Task<IActionResult> EditEvent([FromBody]ClientEventModel model)
    {
      //map to real
      var updatedEvent = _mapper.Map<Event>(model);
      
      //create request
      var request = new EditEventRequest {Event = updatedEvent};
      
      //send request
      var result = await _mediator.Send(request);

      //return OK or bad request 
      if (result)
        return Ok();
      return BadRequest();
      
      throw new NotImplementedException();
    }
    
    
    
    
  
  

  }
}
