using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AnnualTitanium.ClientModels;
using AnnualTitanium.ClientModels.ClientRequests;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace AnnualTitanium.Host.Controllers {

  [ApiController]
  [Route("/api/v1/user")]
  public class UserController : ControllerBase {
  
    public UserController() {}
  
  
    [HttpPost]
    public Task<IActionResult> CreateUser([FromBody]CreateUserRequest request)
    {
      throw new NotImplementedException();
    }

    [HttpDelete]
    public Task<IActionResult> DeleteUser([FromQuery] string email)
    {
      throw new NotImplementedException();
    }

    [HttpGet]
    public Task<IActionResult> GetUser([FromQuery] string email)
    {
      throw new NotImplementedException();
    }

    [HttpPatch]
    public Task<IActionResult> EditUser([FromBody] ClientApplicationUser patchedUser)
    {
      throw new NotImplementedException();
    }
  

  }
}
