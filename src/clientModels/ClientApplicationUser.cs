namespace AnnualTitanium.ClientModels
{
    public record ClientApplicationUser
    {
        public string Id;
        public string Email;
        public string Password;
    }
}