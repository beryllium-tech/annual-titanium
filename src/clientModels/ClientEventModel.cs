using System;

namespace AnnualTitanium.ClientModels
{
    public record ClientEventModel
    {
        public string Id;
        public string Title;
        public DateTime When;

        public string ShortDescription;
        public string MarkdownDescription;
    }
}