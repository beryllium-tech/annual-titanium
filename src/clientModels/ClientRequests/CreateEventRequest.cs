using System;

namespace AnnualTitanium.ClientModels.ClientRequests
{
    public record CreateEventRequest
    {
        public string Title;
        
        public long HoursSinceZero;
        public DateTime When;

        public string ShortDescription;
        public string MarkdownDescription;
    }
}