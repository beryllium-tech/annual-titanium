namespace AnnualTitanium.ClientModels.ClientRequests
{
    public record CreateUserRequest(string Email, string Password);
}