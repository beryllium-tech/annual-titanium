namespace AnnualTitanium.ClientModels.ClientRequests
{
    public record ModerationBoardSpecification(bool DoRatings, bool DoModeration);
}