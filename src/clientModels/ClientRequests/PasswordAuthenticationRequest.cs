namespace AnnualTitanium.ClientModels.ClientRequests
{
    public record PasswordAuthenticationRequest(string Email, string Password);
}