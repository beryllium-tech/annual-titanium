namespace AnnualTitanium.ClientModels.ClientRequests
{
    public record ReportEventRequest(string EventId, string Complaint);
}