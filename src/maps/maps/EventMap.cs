using AnnualTitanium.ClientModels;
using AnnualTitanium.ClientModels.ClientRequests;
using AnnualTitanium.Host.MediatrModels.requests;
using AnnualTitanium.Repository.models;
using AutoMapper;

namespace AnnualTitanium.Maps.maps
{
    public class EventMap : Profile
    {
        public EventMap()
        {
            CreateMap<Event, ClientEventModel>();
            
            CreateMap<ClientEventModel, Event>();
            
            CreateMap<AddEventRequest, Event>(); //TODO: bad practice add method or extension method
            
            CreateMap<CreateEventRequest, AddEventRequest>();//this
        }
    }
}