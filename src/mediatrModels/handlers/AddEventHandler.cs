using System.Threading;
using System.Threading.Tasks;
using AnnualTitanium.ClientModels;
using AnnualTitanium.Host.MediatrModels.requests;
using AnnualTitanium.Repository;
using AnnualTitanium.Repository.models;
using AnnualTitanium.Repository.Repositories;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace AnnualTitanium.Host.MediatrModels.handlers
{
    public class AddEventHandler : IRequestHandler<AddEventRequest, ClientEventModel>
    {
        private readonly IEventRepository _eventRepository;
        private readonly IMapper _mapper;
        private readonly ApplicationIdentityDbContext _dbContext;

        public AddEventHandler(IEventRepository eventRepository, IMapper mapper, ApplicationIdentityDbContext dbContext)
        {
            _eventRepository = eventRepository;
            _mapper = mapper;
            _dbContext = dbContext;
        }

        public async Task<ClientEventModel> Handle(AddEventRequest request, CancellationToken cancellationToken)
        {
            var context = _mapper.Map<Event>(request);

            var result = await _eventRepository.CreateItem(context);

            var item = await _dbContext.Events.FirstOrDefaultAsync(x => x.Id == context.Id, cancellationToken);

            if (!result)
                return null;

            var output = _mapper.Map<ClientEventModel>(context);
            return output;
        }
    }
}