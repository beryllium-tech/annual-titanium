using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AnnualTitanium.Host.MediatrModels.requests;
using AnnualTitanium.Repository;
using AnnualTitanium.Repository.models;
using AnnualTitanium.Repository.Repositories;
using MediatR;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace AnnualTitanium.Host.MediatrModels.handlers
{
    public class DeleteEventHandler : IRequestHandler<DeleteEventRequest, bool>
    {
        private readonly ApplicationIdentityDbContext _dbContext;
        private readonly ILogger<DeleteEventHandler> _logger;
        private readonly IEventRepository _eventRepository;

        public DeleteEventHandler(ApplicationIdentityDbContext dbContext, ILogger<DeleteEventHandler> logger, IEventRepository eventRepository)
        {
            _dbContext = dbContext;
            _logger = logger;
            _eventRepository = eventRepository;
        }
        
        public async Task<bool> Handle(DeleteEventRequest request, CancellationToken cancellationToken)
        {
                  //remove with repo
                  await _eventRepository.DeleteItem(new Event {Id = request.Id});
                  
                  //check dbContext
                  var stillExists = await _dbContext.Events.AnyAsync(x => x.Id == request.Id, cancellationToken);
                  
                  //return boolean
                  if (stillExists)
                  {
                      _logger.LogError("Unable to delete event {Id}", request.Id);
                      return false;
                  }
                  else
                  {
                      _logger.LogInformation("Deleted event {Id}", request.Id);
                      return true;
                  }
        }
    }
}