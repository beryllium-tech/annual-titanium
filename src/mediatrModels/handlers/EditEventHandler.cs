using System.Threading;
using System.Threading.Tasks;
using AnnualTitanium.Host.MediatrModels.requests;
using AnnualTitanium.Repository.Repositories;
using MediatR;
using Microsoft.Extensions.Logging;

namespace AnnualTitanium.Host.MediatrModels.handlers
{
    public class EditEventHandler : IRequestHandler<EditEventRequest, bool>
    {
        private readonly IEventRepository _eventRepository;
        private readonly ILogger<EditEventHandler> _logger;

        public EditEventHandler(IEventRepository eventRepository, ILogger<EditEventHandler> logger)
        {
            _eventRepository = eventRepository;
            _logger = logger;
        }
        
        public async Task<bool> Handle(EditEventRequest request, CancellationToken cancellationToken)
        {
            //_eventRepo
            var result = await _eventRepository.EditItem(request.Event);

            //check return


            if (result)
                _logger.LogInformation("Successfully edited event {@Event}", request.Event);
            else
                _logger.LogError("Not able to edit event {@Event}", request.Event);
    
            //return
            return result;
            
        }
    }
}