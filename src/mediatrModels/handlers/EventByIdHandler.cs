using System.Threading;
using System.Threading.Tasks;
using AnnualTitanium.ClientModels;
using AnnualTitanium.Host.MediatrModels.requests;
using AnnualTitanium.Repository;
using AnnualTitanium.Repository.Repositories;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace AnnualTitanium.Host.MediatrModels.handlers
{
    public class EventByIdHandler : IRequestHandler<EventByIdRequest, ClientEventModel>
    {
        private readonly ApplicationIdentityDbContext _dbContext;
        private readonly IMapper _mapper;
        private readonly ILogger<EventByIdHandler> _logger;

        public EventByIdHandler(ApplicationIdentityDbContext dbContext, IMapper mapper, ILogger<EventByIdHandler>logger)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _logger = logger;
        }
        
        public async Task<ClientEventModel> Handle(EventByIdRequest request, CancellationToken cancellationToken)
        {
            
            //get through repo
            var result = await _dbContext.Events.FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);
            
            //if ! exists ? return
            if (result == null)
                return null;

            //make to client client model
            var output = _mapper.Map<ClientEventModel>(result);
            
            if (output != null)
                _logger.LogInformation("Fetched {} event from dbContext", output);
            else
                _logger.LogError("No event with id {} existed", request.Id);
            

                //return
            return output;
        }
    }
}