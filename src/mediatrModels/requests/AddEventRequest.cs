using System;
using AnnualTitanium.ClientModels;
using MediatR;

namespace AnnualTitanium.Host.MediatrModels.requests
{
    public class AddEventRequest : IRequest<ClientEventModel>
    {
        public string Title;
        public DateTime When;

        public string ShortDescription;
        public string MarkdownDescription;
    }
}