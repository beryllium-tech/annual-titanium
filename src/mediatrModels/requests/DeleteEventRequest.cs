using MediatR;

namespace AnnualTitanium.Host.MediatrModels.requests
{
    public class DeleteEventRequest : IRequest<bool>
    {
        public string Id { get; set; }
    }
}