using AnnualTitanium.Repository.models;
using MediatR;

namespace AnnualTitanium.Host.MediatrModels.requests
{
    public class EditEventRequest : IRequest<bool>
    {
        public Event Event { get; set; }
    }
}