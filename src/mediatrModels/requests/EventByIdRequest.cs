using AnnualTitanium.ClientModels;
using MediatR;

namespace AnnualTitanium.Host.MediatrModels.requests
{
    public class EventByIdRequest : IRequest<ClientEventModel>
    {
        public string Id { get; init; }
    }
}