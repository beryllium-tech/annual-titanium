using AnnualTitanium.Repository.models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace AnnualTitanium.Repository
{
    public class ApplicationIdentityDbContext : IdentityDbContext<ApplicationIdentityUser>
    {

        public ApplicationIdentityDbContext(DbContextOptions<ApplicationIdentityDbContext> options) : base(options)
        {
            
        }
        
        
        public DbSet<Event> Events { get; set; }
        public DbSet<Report> Reports { get; set; }
    }
}
