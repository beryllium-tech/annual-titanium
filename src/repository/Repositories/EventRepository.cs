using System.Threading.Tasks;
using System.Xml.Xsl;
using AnnualTitanium.Repository.models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace AnnualTitanium.Repository.Repositories
{
    public interface IEventRepository : IRepository<Event> { }
    public class EventRepository : IEventRepository
    {
        private readonly ApplicationIdentityDbContext _dbContext;
        private readonly ILogger<EventRepository> _logger;

        public EventRepository(ApplicationIdentityDbContext dbContext, ILogger<EventRepository> logger)
        {
            _dbContext = dbContext;
            _logger = logger;
        }

        public async Task<bool> CreateItem(Event context)
        {
            var result = await _dbContext.Events.AddAsync(context);
            await _dbContext.SaveChangesAsync();

            if (result != null)
            {
                _logger.LogInformation("Created {@Event} event successfully", context);
                return true;
            }
            else
            {
                _logger.LogError("Failed to create {@Event} event", context);
                return false;
            }
        }

        public async Task DeleteItem(Event context)
        {
            var result = await _dbContext.Events.FirstOrDefaultAsync(x => x.Id == context.Id);
            if (result == null)
            {
                _logger.LogError("Not able to delete non existent event {@Event}", context);
                return;
            }
                

            _dbContext.Events.Remove(result);
            await _dbContext.SaveChangesAsync();
            _logger.LogInformation("Removed {@Event} event", result);
        }


        public async Task<bool> EditItem(Event context)
        {
            var result = await _dbContext.Events.FirstOrDefaultAsync(x => x.Id == context.Id);
            if (result == null)
            {
                _logger.LogError("Unable to edit non existent event {@Event}", context);
                return false;
            }
                

            result = result with
            {
                Title = context.Title,
                When = context.When,
                ShortDescription = context.ShortDescription,
                MarkdownDescription = context.MarkdownDescription
            };

            var updateResult = _dbContext.Events.Update(result);
            await _dbContext.SaveChangesAsync();
            _logger.LogInformation("Edited {@Event} event", result);
            return true;
        }
    }
}