using System.Threading.Tasks;
using AnnualTitanium.Repository.models;

namespace AnnualTitanium.Repository.Repositories
{
    public interface IRepository<T>
    {
        Task<bool> CreateItem(T context);
        Task DeleteItem(T context);
        Task<bool> EditItem(T context);
    }
}