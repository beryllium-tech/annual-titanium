using System.Threading.Tasks;
using AnnualTitanium.Repository.models;
using Microsoft.EntityFrameworkCore;

namespace AnnualTitanium.Repository.Repositories
{
    
    public interface IReportsRepository : IRepository<Report> { }
    
    public class ReportsRepository : IReportsRepository
    {
        private readonly ApplicationIdentityDbContext _dbContext;

        public ReportsRepository(ApplicationIdentityDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        public async Task<bool> CreateItem(Report context)
        {
            var result = await _dbContext.Reports.AddAsync(context);
            return result != null;
        }

        public async Task DeleteItem(Report context)
        {
            var result = await _dbContext.Reports.FirstOrDefaultAsync(x => x.Id == context.Id);
            if (result == null)
                return;

            _dbContext.Remove(context);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<bool> EditItem(Report context)
        {
            var result = await _dbContext.Reports.FirstOrDefaultAsync(x => x.Id == context.Id);
            if (result == null)
                return false;

            result = result with
            {
                EventId = context.EventId,
                Reasoning = context.Reasoning
            };

            _dbContext.Reports.Update(result);
            await _dbContext.SaveChangesAsync();
            return true;
        }
    }
}