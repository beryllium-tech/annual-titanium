using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace AnnualTitanium.Repository.Repositories
{
    public interface IUserRepository : IRepository<ApplicationIdentityUser> { }

    public class UserRepository : IUserRepository
    {
        private readonly ApplicationIdentityDbContext _dbContext;

        public UserRepository(ApplicationIdentityDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        public async Task<bool> CreateItem(ApplicationIdentityUser context)
        {
            var result = await _dbContext.Users.AddAsync(context);
            await _dbContext.SaveChangesAsync();
            
            return result != null;
        }

        public async Task DeleteItem(ApplicationIdentityUser context)
        {
            var result = await _dbContext.Users.FirstOrDefaultAsync(x => x.Id == context.Id);
            if (result == null)
                return;

            _dbContext.Users.Remove(result);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<bool> EditItem(ApplicationIdentityUser context)
        {
            var result = await _dbContext.Users.FirstOrDefaultAsync(x => x.Id == context.Id);
            if (result == null)
                return false;
            
            result.Email = context.Email;
            result.UserName = context.UserName;

            _dbContext.Users.Update(result);
            await _dbContext.SaveChangesAsync();

            return true;
        }
    }
}