using System;
using System.ComponentModel.DataAnnotations;

namespace AnnualTitanium.Repository.models
{
    public record Event
    {
        [Key] 
        public string Id { get; set; } = Guid.NewGuid().ToString();

        [DataType(DataType.DateTime)] 
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy-hh:mm}")]
        public DateTime CreatedAt { get; set; }
        
        public string Title { get; set; }
        
        [DataType(DataType.Date)] 
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime When { get; set; }

        public string ShortDescription { get; set; }
        public string MarkdownDescription { get; set; }
    }
}