using System;
using System.ComponentModel.DataAnnotations;

namespace AnnualTitanium.Repository.models
{
    public record Report
    {
        [Key] public string Id { get; set; } = Guid.NewGuid().ToString();

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy-hh:mm}")]
        public DateTime CreatedAt { get; set; }

        public string EventId { get; set; }
        public string Reasoning { get; set; }
    }
}